# -*- coding: utf-8 -*-
#!/usr/bin/env python3

import json
from flask import Flask, render_template, url_for, jsonify, request, abort
from flask_cors import CORS
import toolforge
import requests
import re
from config import siteData, domains

# Test log
import logging
logging.basicConfig(filename="newfile.log", 
                    format='%(asctime)s %(message)s', 
                    filemode='w')
logger=logging.getLogger()
logger.setLevel(logging.DEBUG)

version = "2.0"

S = requests.Session()

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    jsonFile = open("Stats.json", "r")  # Open the JSON file for reading
    data = json.load(jsonFile)  # Read the JSON into the buffer
    jsonFile.close()  # Close the JSON file

    return render_template('index.html', domains= domains, data= data)

@app.route('/wikitable')
def wikitable():

    jsonFile = open("Stats.json", "r")  # Open the JSON file for reading
    Jsondata = json.load( jsonFile )  # Read the JSON into the buffer
    jsonFile.close()  # Close the JSON file

    Wikitable = "Statistics on "+ Jsondata[ 'timestamp']
    Wikitable += """
{|class="wikitable sortable"
|-
! colspan="7" style="text-align:center;background: #ffffff;" | Page namespace
! colspan="4" style="text-align:center;background: #ffffff;" | Main namespace
|-
!style="background: #ffffff;"|'''Language'''
!style="background: #ffffff;"|'''All pages'''
!style="background: #ddd;"|'''Without text'''
!style="background: #ffa0a0;"|'''Not proofread'''
!style="background: #b0b0ff;"|'''Problematic'''
!style="background: #ffe867;"|'''Proofread'''
!style="background: #90ff90;"|'''Validated'''
!style="background: #ffffff;"|'''All pages'''
!style="background: #90ff90;"|'''With scans'''
!style="background: #ffa0a0;"|'''Without scans'''
!style="background: #ffffff;"|'''%'''"""

    Jsondata.pop('timestamp', None)

    # Sorting
    Jsondata = json.dumps(Jsondata, sort_keys=True)
    Jsondata = json.loads( Jsondata )

    for domain in domains:

        Wikitable += """\n|-
|%s || %d || %d || %d || %d || %d || %d || %d || %d || %d || %.2f""" % (
            domain,
            Jsondata[domain]['Num_of_pages'],
            Jsondata[domain]['Without_text'],
            Jsondata[domain]['Not_proofread'],
            Jsondata[domain]['Problematic'],
            Jsondata[domain]['Proofread'],
            Jsondata[domain]['Validated'],
            Jsondata[domain]['Main_Pages'],
            Jsondata[domain]['Main_WithScan'],
            Jsondata[domain]['Main_WithOutScan'],
            100 * Jsondata[domain]["Main_WithScan"] / (Jsondata[domain]["Main_WithScan"] + Jsondata[domain]["Main_WithOutScan"])
        )

    Wikitable +="\n|}"
    return render_template('wikitable.html', Wikitable= Wikitable)

# API
@app.route('/api/stats')
def statsAPI():
    jsonFile = open("Stats.json", "r")  # Open the JSON file for reading
    Jsondata = json.load( jsonFile )  # Read the JSON into the buffer
    jsonFile.close()  # Close the JSON file

    return jsonify( Jsondata )

@app.route('/monthly')
def monthly():
    time = '201912'
    dbname = 'bnwikisource_p'
    conn = toolforge.connect( dbname )
    cur = conn.cursor()

    # Get Active Users
    active_users = []
    active_q = "select DISTINCT(actor_name) as users " \
        "from revision inner join actor on actor.actor_id = rev_actor " \
        "where rev_timestamp > '"+ time + "01000000' and rev_timestamp < '" + time + "31235959';"

    cur.execute(active_q)
    active_rows = cur.fetchall()
    for row in active_rows:
        active_users.append( str(row[0].decode("utf-8")) )

    return render_template('monthly.html', active_users= active_users)

@app.route('/graph')
def graph():
    return render_template('graph.html')

@app.route('/usercontrib', methods = ['POST', 'GET'])
def usercontrib():

    if request.method == 'GET':
        return render_template('usercontrib.html', data=None, errors= None, prContrib = None, valContrib = None)

    elif request.method == 'POST':
        data = {}

        prContrib = None
        valContrib = None

        ws_project = request.form['ws-project']
        ws_usr = request.form['ws-usr']
        ws_usr = ws_usr.strip()


        pageNS = str(siteData[ws_project]["namespace"]["page"])
        indexNS = str(siteData[ws_project]["namespace"]["index"])

        # Test log
        logger.debug(ws_project + ' ' + ws_usr)
        logger.debug('Namespace:-' + str(pageNS) + ' ' + str(indexNS))

        # Connect to database
        conn = toolforge.connect( ws_project +'wikisource')

        # Get the actor id
        actid = ""
        with conn.cursor() as cur:
            cur.execute('select * from actor where actor_name = "' + ws_usr + '"')
            try:
                actid = cur.fetchone()[0]
            except TypeError as e:
                return render_template('usercontrib.html', data=None, errors= e)

        # Test log
        logger.debug('actid ' + str(actid) )

        # Edit counts
        with conn.cursor() as cur:

            # Live Edits
            cur.execute('select count(rev_id) from revision_userindex where rev_actor =' + str(actid) )
            data["liveEdit"] = cur.fetchone()[0]

            # Deleted Edits
            cur.execute('select count(*) from archive where ar_actor=' + str(actid) )
            data["delEdit"] = cur.fetchone()[0]

            # Total Edits
            data["totalEdit"] = data["liveEdit"] + data["delEdit"]

            # Page Namespace Edits
            cur.execute("select count(rev_id) " \
                "from revision_userindex ru " \
                "inner join page p on rev_page = page_id " \
                "where rev_actor=" + str(actid) + \
                " and page_namespace =" + pageNS)
            data["pageEdit"] = cur.fetchone()[0]

            # Index Namespace Edits
            cur.execute("select count(rev_id) " \
                "from revision_userindex ru " \
                "inner join page p on rev_page = page_id " \
                "where rev_actor=" + str(actid) + \
                " and page_namespace ="+ indexNS)
            
            data["indexEdit"] = cur.fetchone()[0]

        qry = "select rev_id" \
            " from revision_userindex ru" \
            " inner join page p on rev_page = page_id" \
            " where rev_actor=" + str(actid) + \
            " and rev_timestamp > 20190100000000" \
            " and page_is_redirect = 0" \
            " and page_namespace =" + pageNS

        # Test log
        logger.debug('qry ' + str(qry) )

        with conn.cursor() as cur:
            cur.execute(str(qry))
            row = cur.fetchall()

            # Test log
            logger.debug('row ' + str(row) )

            if ( row != () ):

                # Creating a list
                lst = []
                for i in row:
                    lst.append( i[0] )

                # Dividing into chunks
                chunk = [
                    lst[i:i+49]
                    for i in range(0, len(lst), 49)
                ]

                urlEndpoint = 'https://' + ws_project + '.wikisource.org/w/api.php'
                prCounter = 0
                valCounter = 0
                prContrib = {}
                valContrib = {}

                for i in chunk:
                    ids = '|'.join( map(str, i) )

                    logger.debug('ids ' + str(ids) )

                    param = {
                        "action": "query",
                        "utf8": 1,
                        "prop": "revisions",
                        "revids": ids,
                        "rvprop": "content|user|ids",
                        "rvslots": "main",
                        "formatversion": "2",
                        "format": "json"
                    }

                    # Convert response into JSON
                    R = S.get(url=urlEndpoint, params=param)

                    try:
                        Resdata = R.json()
                        pages =  Resdata["query"]["pages"]

                        for page in pages:
                            for rev in page["revisions"]:
                                content = rev["slots"]["main"]["content"]
                                matches = re.findall(r'<pagequality level="(\d)" user="(.*?)" />', content)
                                if( matches == []
                                    or ( int(matches[0][0]) != 3 and int(matches[0][0]) != 4 )
                                    or ( ws_usr != str(matches[0][1]) )
                                ):
                                    continue

                                if ( int(matches[0][0]) == 3 ):
                                    if page["title"] in prContrib:
                                        prCounter = prCounter
                                    else:
                                        prCounter = prCounter + 1
                                    prContrib[page["title"]] = rev["revid"]

                                if ( int(matches[0][0]) == 4 ):
                                    if page["title"] in valContrib:
                                        valCounter = valCounter
                                    else:
                                        valCounter = valCounter + 1
                                    valContrib[page["title"]] = rev["revid"]
                    except:
                        data["pr"] = 'Something went wrong with page revisions'
                        data["val"] = 'Something went wrong with page revisions'
                        return render_template('usercontrib.html', data= data, errors=None)

                # Set the proofread and validation counter
                data["pr"] = prCounter
                data["val"] = valCounter
            else:
                # Set the proofread and validation counter 0 as row tuple is empty
                data["pr"] = 0
                data["val"] = 0
                prContrib = None
                valContrib = None

        return render_template('usercontrib.html', data= data, errors=None, prContrib=prContrib, valContrib= valContrib)

    else:
        abort(400)


if __name__ == '__main__':
    app.run()
