$(function() {

	$("#ws-usr").autocomplete({
		source: function (request, response) {
			inputValue = request.term
            $.ajax( {
                'url': 'https://'+ $("#ws-project").val() +'.wikisource.org/w/api.php',
                'data': {
                    'action': 'query',
                    'list': 'allusers',
                    'aulimit': 10,
                    'auprefix': inputValue[ 0 ].toUpperCase() + inputValue.slice( 1 ),
                    'origin': '*',
                    'format': 'json'
                },
                'success': function( data ) {
                    response( $.map(data.query.allusers, function(resUser){
                        return resUser.name;
                    }));
                }
			});
		}
    });

    $('#form-content').on('submit', function () {
        // Remove focus from any active element
        document.activeElement.blur();

        // Disable the form so they can't hit Enter to re-submit
        $('.subBtn').prop('readonly', true);

        // Show the waiting Msg
        $('#waitMsg').css('display','block');

        // Change the submit button text.
        $('.subBtn').prop('disabled', true)
            .html( "Loading <span id='submit_timer'></span>");

        // Add the counter.
        var startTime = Date.now();
        setInterval(function () {
            var elapsedSeconds = Math.round((Date.now() - startTime) / 1000);
            var minutes = Math.floor(elapsedSeconds / 60);
            var seconds = ('00' + (elapsedSeconds - (minutes * 60))).slice(-2);
            $('#submit_timer').text(minutes + ":" + seconds);
        }, 1000);
    });

    $('.leg-contrib').on('click', function(){
         $( this ).toggleClass( "fa-angle-up" ).toggleClass( "fa-angle-down" );
    });
});